# Assignment 2 - Web App Development Server Side

Name: Yuting Jing
Student ID: 20086428

## API endpoints.

 + GET /users - Get all users.
 + GET /users/:id - Get a user by id.
 + GET /usersLogout - Get the user that logged in to logout.
 + GET /poems - Get all poems.
 + GET /poems/:id - Get all poems by id.
 + GET /mypoems -Get current user's poems (**NEW**)
 + GET /authors - Get all authors.
 + GET /authors/:id - Get all authors by id.
 + POST /usersRegister - Let user register.
 + POST /users/login - Let user login.
 + POST /poems - Add a poem.
 + POST /authors - Add an author.
 + PUT /authors/:id/work - Add a work for an author's works array
 + PUT /authors/:id/deletework - Delete a work for an author's works array
 + PUT /authors/:id/like - Give a author a like  (**IMPROVED**)
 + PUT /authors/:id/unlike - Cancel a like of an author (**IMPROVED**)
 + PUT /poems/:id/like - Give a poem a like (**IMPROVED**)
 + PUT /poems/:id/unlike - Cancel a like of a poem (**IMPROVED**)
 + PUT /poems/:id - Edit a poem (**NEW**)
 + DELETE /users/:id - Delete a user from the database.
 + DELETE /poems/:id - Delete a poem from the database.
 + DELETE /authors/:id - Delete an author from the database.


## Class Diagram.

![][datamodel]

***More information please check Design Document.doc in zip***

[datamodel]: ./img/data_model.png
